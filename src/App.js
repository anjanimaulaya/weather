import "./App.css";
import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import LayoutWeather from "./containers/LayoutWeather";

class App extends Component {
  render() {
    return (
      <div className="App">
        <LayoutWeather/>
      </div>
    )
  }
}

export default App;