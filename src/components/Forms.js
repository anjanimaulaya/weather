import React from "react";
import {Form, Button} from 'react-bootstrap';

function Forms(props) {
	return (
		<Form onSubmit={props.getWeather}>
			<Form.Group>
				<Form.Label>City</Form.Label>
				<Form.Control type="text" name="city" placeholder="Enter your city"/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Country</Form.Label>
				<Form.Control type="text" name="country" placeholder="Enter your country"/>
			</Form.Group>
			<Button variant = "primary" type="submit">
				Get Weather
			</Button>
		</Form>
	)
}

export default Forms;