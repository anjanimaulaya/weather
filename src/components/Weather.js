import React from "react";
import { Card } from "react-bootstrap";

function Weather(props) {
  return (
    <div align="center">
      <Card style={{ width: "25rem" }}>
        {props.city && props.country && (
          <p>
            Location: {props.city}, {props.country}
          </p>
        )}
        {props.temperature && <p> Temperature:{props.temperature}</p>}
        {props.humidity && (
          <p>
            Humidity:
            {props.humidity}
          </p>
        )}
        {props.description && (
          <p>
            Conditions:
            {props.description}
          </p>
        )}
        {props.error && <p>{props.error}</p>}
      </Card>
    </div>
  );
}

export default Weather;
