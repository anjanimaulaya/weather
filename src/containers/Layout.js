import React, { Component } from 'react'
import Header from './Header'
import LayoutWeather from './LayoutWeather'

class Layout extends Component {
    render() {
        return (
            <div className="Header">
                <Header/>
                <LayoutWeather/>
            </div>
        )
    }
}

export default Layout;