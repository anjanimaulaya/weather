import React from 'react'
import { Jumbotron } from 'react-bootstrap'

function Header() {
    return (
        <div>
            <Jumbotron fluid>
                <h1>CHECK CURRENT WEATHER HERE</h1>
                <p>Find out your city's weather</p>
            </Jumbotron>
        </div>
    )
}

export default Header;