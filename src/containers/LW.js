import React from "react";
import axios from 'axios'
import News from "../common/News/News";
import Weather from "../common/Weather/Weather";

const API_KEY_WEATHER = "9d1c07f5460fb02f397103a767940809";

class Home extends React.Component{
    state ={
        weather: [],
        temp: [],
        clouds: []
    }

    getWeather = query => {
        axios.get(`https://api.openweathermap.org/data/2.5/find?q=${query}&units=imperial&appid=${API_KEY_WEATHER}`)
          .then(response => {
            this.setState({
              weather: response.data.list[0],
              temp: response.data.list[0].main.temp,
              clouds: response.data.list[0].weather[0].description
            });
          })
          .catch(error => {
            console.log('Error', error);
          });
      };

    queryWeather = (event, cityName) => {
    if (event.key === 'Enter') {
        event.preventDefault();
        cityName = event.target.value;
        this.getWeather(cityName);
    }
    }

    render() {
    
    const getcity = ()=> {
        if(this.state.weather.name){
            return "City : " + this.state.weather.name
        }  
    }
    const gettemp = () => {
        if(this.state.temp){
            return "Temperature : " + this.state.temp
        }
    }
    const getsky = () => {
        if(this.state.clouds){
            return "Sky : " + this.state.clouds
        }
    }
        return (
            <div>
                <br/>
                <div className="row">
                    <div className="col-md-8">
                        {this.state.news.map(berita =>{
                            return <News
                                key={new Date()}
                                title={berita.title}
                                description={berita.description}
                                url={berita.url}
                            />
                        })}
                    </div>
                    <div className="col-md-4">
                        
                        <Weather queryWeather={this.queryWeather} city={getcity()} temp={gettemp()} clouds={getsky()}/>
                    </div>
                </div>
            </div>
        );
    }
}
export default Home;