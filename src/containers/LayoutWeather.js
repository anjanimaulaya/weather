import React, { Component } from "react";
import Forms from "../components/Forms";
import Weather from "../components/Weather";
import Header from "./Header";
import { FormGroup } from "react-bootstrap";

const API_KEY = "fd82985a61e52ec6f7aad8360db9da01";

class LayoutWeather extends Component {
  state = {
    temperature: undefined,
    city: undefined,
    country: undefined,
    humidity: undefined,
    description: undefined,
    error: undefined
  };

  getWeather = async e => {
    e.preventDefault();
    const city = e.target.elements.city.value;
    const country = e.target.elements.country.value;
    const api_call = await fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`
    );
    const data = await api_call.json();
    if (city && country) {
      this.setState({
        temperature: data.main.temp,
        city: data.name,
        country: data.sys.country,
        humidity: data.main.humidity,
        description: data.weather[0].description,
        error: ""
      });
    } else {
      this.setState({
        temperature: undefined,
        city: undefined,
        country: undefined,
        humidity: undefined,
        description: undefined,
        error: "Data can't be null"
      });
    }
  };

  render() {
    return (
      <div>
        <Header></Header>
        <FormGroup>
          <Forms getWeather={this.getWeather} />
        </FormGroup>
        <FormGroup>
          <Weather
            temperature={this.state.temperature}
            humidity={this.state.humidity}
            city={this.state.city}
            country={this.state.country}
            description={this.state.description}
            error={this.state.error}
          />
        </FormGroup>
      </div>
    );
  }
}

export default LayoutWeather;
